Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FCM, Flexible Configuration Manager
Upstream-Contact: FCM Team <fcm-team@metoffice.gov.uk>
Source: http://www.metoffice.gov.uk/research/collaboration/fcm

Files: *
Copyright: British Crown Copyright 2006-15 Met Office.
License: GPL-3

Files: doc/*
Copyright: British Crown Copyright 2006-15 Met Office.
License: Open-Government

Files: doc/etc/bootstrap/*
Copyright: 2013 Twitter Inc.
License: Apache-2.0

Files: svn-hooks/svnperms.py
Copyright: Gustavo Niemeyer <gustavo@niemeyer.net>
License: Apache-2.0

Files: etc/moment.min.js
Copyright: Tim Wood
License: MIT

Files: debian/missing-sources/moment.js
Copyright: Tim Wood
License: MIT

License: GPL-3
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation;
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.

License: Open-Government
 You may use and re-use Crown copyright information from any part of the
 FCM Documentation (not including logos) free of charge in
 any format or medium, under the terms and conditions of the 
 Open Government License (http://www.nationalarchives.gov.uk/doc/open-government-licence/)
 , provided it is reproduced accurately
 and not used in a misleading context. Where any of the Crown copyright
 items on this website are being republished or copied to others, the source
 of the material must be identified and the copyright status
 acknowledged.
 .
 See the Open Government Licence
 (http://www.nationalarchives.gov.uk/doc/open-government-licence/)
 for the full conditions of this licence and for information 
 on how to attribute the source of material.
 .
 The FCM software and FCM Documentation are maintained
 by the FCM Team (mail:fcm-team@metoffice.gov.uk), Met Office,
 FitzRoy Road, Exeter, EX1 3PB, UK.

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the full text of the Apache
 License version 2 can be found in the file
 `/usr/share/common-licenses/Apache-2.0'.

License: MIT
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation files 
 (the "Software"), to deal in the Software without restriction, 
 including without limitation the rights to use, copy, modify, merge, 
 publish, distribute, sublicense, and/or sell copies of the Software, 
 and to permit persons to whom the Software is furnished to do so, 
 subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

